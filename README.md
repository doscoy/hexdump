# hexdump

標準出力にhexdump

## 使い方

サンプルデータ
```cpp
constexpr int data[]{0, 1, 2, 3, 1667391776};
constexpr int bytes{sizeof(data)};
```

---

ポインタとサイズを渡す
```cpp
std::cout << hexdump::print(data, bytes);
```
<pre>0x00000000: 00 00 00 00 01 00 00 00 02 00 00 00 20 61 62 63             ▭abc
</pre>

---

先頭のアドレスを指定する
```cpp
std::cout << hexdump::print(data, bytes, 0x5678);
```
<pre>0x00005670:                         00 00 00 00 01 00 00 00                 
0x00005680: 02 00 00 00 20 61 62 63                             ▭abc
</pre>

---

文字列を渡すと検索して強調表示
```cpp
std::cout << hexdump::print(data, bytes, "abc");
```
<pre>0x00000000: 00 00 00 00 01 00 00 00 02 00 00 00 20 <b>61 62 63</b>             ▭<b>abc</b>
</pre>

---

`initializer_list<unsigned char>`を渡すと検索して強調表示
```cpp
std::cout << hexdump::print(data, bytes, {0x01, 0x00});
```
<pre>0x00000000: 00 00 00 00 <b>01 00</b> 00 00 02 00 00 00 20 61 62 63             ▭abc
</pre>

---

アドレス指定と検索の組み合わせ
```cpp
std::cout << hexdump::print(data, bytes, 0x5678, "abc");
std::cout << hexdump::print(data, bytes, 0x5678, {0x01, 0x00});
```
<pre>0x00005670:                         00 00 00 00 01 00 00 00                 
0x00005680: 02 00 00 00 20 <b>61 62 63</b>                             ▭<b>abc</b>
0x00005670:                         00 00 00 00 <b>01 00</b> 00 00                 
0x00005680: 02 00 00 00 20 61 62 63                             ▭abc
</pre>
