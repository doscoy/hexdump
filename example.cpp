#include <iostream>

#include "hexdump.hpp"

int main(int, char**) {
  constexpr char src[]{
      "O, beware, my lord, of jealousy;\n"
      "It is the green-eyed monster which doth mock\n"
      "The meat it feeds on; that cuckold lives in bliss\n"
      "Who, certain of his fate, loves not his wronger;\n"
      "But, O, what damned minutes tells he o'er\n"
      "Who dotes, yet doubts, suspects, yet strongly loves!\n"};
  constexpr auto bytes{sizeof(src)};

  // 1. basic usage
  std::cout << hexdump::print(src, bytes) << std::endl;

  // 2. address shift
  std::cout << hexdump::print(src, bytes, 0x8008) << std::endl;

  // 3. word search
  std::cout << hexdump::print(src, bytes, "loves") << std::endl;

  // 4. hex search
  std::cout << hexdump::print(src, bytes, {0x2C, 0x20}) << std::endl;

  return 0;
}
