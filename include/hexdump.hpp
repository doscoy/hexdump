#pragma once

#include <algorithm>
#include <initializer_list>
#include <iomanip>
#include <ostream>
#include <string_view>

namespace hexdump {

class print {
  friend std::ostream& operator<<(std::ostream& os, const print& dump);

 public:
  print() = delete;
  print(const print&) = delete;
  print(print&&) = delete;
  print& operator=(const print&) = delete;
  print& operator=(print&&) = delete;

  print(const void* const data, const int size, const int addr,
        const std::string_view& search)
      : addr_{std::max(addr, 0)},
        data_{static_cast<const char*>(data),
              static_cast<std::string_view::size_type>(std::max(size, 0))},
        search_{search} {}

  print(const void* const data, const int size, const int addr,
        const std::initializer_list<unsigned char>& search)
      : print(data, size, addr,
              {reinterpret_cast<const char*>(search.begin()),
               static_cast<std::string_view::size_type>(search.size())}) {}

  print(const void* const data, const int size, const std::string_view& search)
      : print(data, size, 0, search) {}

  print(const void* const data, const int size,
        const std::initializer_list<unsigned char>& search)
      : print(data, size, 0,
              {reinterpret_cast<const char*>(search.begin()),
               static_cast<std::string_view::size_type>(search.size())}) {}

  print(const void* const data, const int size, const int addr)
      : print(data, size, addr, "") {}

  print(const void* const data, const int size) : print(data, size, 0, "") {}

 private:
  class emphasis {
    static constexpr auto kEmphasisCommand{"\033[31m"};
    static constexpr auto kRemoveCommand{"\033[0m"};

   public:
    emphasis(std::ostream& os, const bool* const flag_list)
        : os_{os}, flag_list_{flag_list}, blocker_{false} {}
    ~emphasis() {
      if (blocker_) {
        os_ << kRemoveCommand;
      }
    }
    void Proc(const int idx) {
      if (not blocker_ && flag_list_[idx]) {
        blocker_ = true;
        os_ << kEmphasisCommand;
      } else if (blocker_ && not flag_list_[idx]) {
        blocker_ = false;
        os_ << kRemoveCommand;
      }
    }

   private:
    std::ostream& os_;
    const bool* const flag_list_;
    bool blocker_;
  };

  const int addr_;
  const std::string_view data_;
  const std::string_view search_;
};

std::ostream& operator<<(std::ostream& os, const print& dump) {
  using diff = decltype(dump.data_.end() - dump.data_.begin());
  constexpr auto kAddrWidth{static_cast<int>(sizeof(int) * 2)};
  constexpr auto kRowSize{16};
  constexpr auto kWhiteSpace{"\u25AD"};

  const auto flag_backup{os.flags()};
  os << std::hex << std::uppercase << std::setfill('0');

  const auto PrintAddr{
      [&os](auto row) { os << "0x" << std::setw(kAddrWidth) << row << ": "; }};

  bool emphasis[kRowSize]{false};
  auto find{dump.search_.size() != 0 ? dump.data_.find(dump.search_)
                                     : std::string_view::npos};
  bool reset_flag{false};
  const auto UpdateEmphasis{
      [&dump, &emphasis, &find, &reset_flag](auto itr, int len) {
        if (reset_flag) {
          std::fill_n(emphasis, kRowSize, false);
          reset_flag = false;
        }
        const auto pos{itr - dump.data_.begin()};
        while (true) {
          if (find == std::string_view::npos) {
            return;
          }
          if (static_cast<diff>(find) >= len + pos) {
            return;
          }
          const auto carry{static_cast<diff>(find) < pos};
          const auto head{carry ? 0 : find - pos};
          const auto emp_len{carry ? dump.search_.size() + find - pos
                                   : dump.search_.size()};
          const auto fix_len{std::min(kRowSize - head, emp_len)};
          for (int i = 0; i < static_cast<int>(fix_len); ++i) {
            emphasis[head + i] = true;
          }
          reset_flag = true;
          if (fix_len == emp_len) {
            find = dump.data_.find(dump.search_, find + dump.search_.size());
          } else {
            return;
          }
        }
      }};

  const auto PrintHex{
      [&os, &emphasis, end = dump.data_.end()](auto itr, auto len) {
        const auto fix_len{std::min(static_cast<diff>(len), end - itr)};
        {
          print::emphasis helper{os, emphasis};
          for (decltype(len) i = 0; i < fix_len; ++i, ++itr) {
            helper.Proc(i);
            const auto put{static_cast<unsigned char>(*itr)};
            os << std::setw(2) << static_cast<int>(put) << " ";
          }
        }
        if (len != fix_len) {
          for (int i = 0; i < (len - fix_len); ++i) {
            os << "   ";
          }
        }
      }};

  const auto PrintAscii{
      [&os, &emphasis, end = dump.data_.end()](auto itr, auto len) {
        const auto fix_len{std::min(static_cast<diff>(len), end - itr)};
        print::emphasis helper{os, emphasis};
        for (decltype(len) i = 0; i < fix_len; ++i, ++itr) {
          helper.Proc(i);
          if (*itr == ' ') {
            os << kWhiteSpace;
          } else if (*itr > ' ' && *itr <= '~') {
            os << *itr;
          } else {
            os << ' ';
          }
        }
      }};

  auto row{(dump.addr_ / kRowSize) * kRowSize};
  auto itr{dump.data_.begin()};

  {
    const auto offset{dump.addr_ % kRowSize};
    const auto print{kRowSize - offset};
    PrintAddr(row);
    UpdateEmphasis(itr, print);
    for (int i = 0; i < offset; ++i) {
      os << "   ";
    }
    PrintHex(itr, print);
    for (int i = 0; i < offset; ++i) {
      os << ' ';
    }
    PrintAscii(itr, print);
    os << '\n';
    row += kRowSize;
    itr += print;
  }

  while (itr < dump.data_.end()) {
    PrintAddr(row);
    UpdateEmphasis(itr, kRowSize);
    PrintHex(itr, kRowSize);
    PrintAscii(itr, kRowSize);
    os << '\n';
    row += kRowSize;
    itr += kRowSize;
  }

  os.flags(flag_backup);
  return os;
}

}  // namespace hexdump